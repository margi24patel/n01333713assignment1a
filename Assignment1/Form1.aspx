﻿<%@ Page Language="C#" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Form1</title>
	<script runat="server">
	
	</script>
</head>
<body>
	<form id="form1" runat="server">
	
       <div>
                
                
                <h1>Hotel Reservation Form</h1>
                
                <asp:label id ="txtname" runat="server" text="Name:"></asp:label>
                <br>
                
                <asp:TextBox runat="server" ID="txtfname" placeholder="First Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter First Name" ControlToValidate="txtfname" id="validatorfname"></asp:RequiredFieldValidator>
                <br>
                
                <asp:TextBox runat="server" id="txtlname" placeholder="Last Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Last Name" ControlToValidate="txtlname" id="validatorlname"></asp:RequiredFieldValidator>
                <br>
                
                <asp:label id="address" runat="server" text="Address"></asp:label>
                <br>
                
                <asp:TextBox runat="server" id="streetadd" placeholder="Street Address"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter StreetAddres" ControlToValidate="streetadd" id="validatorstreetadd"></asp:RequiredFieldValidator>
                <br>
                
                <asp:TextBox runat="server" id="city" placeholder="City Name,State"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter CityName" ControlToValidate="city" id="validatorcity"></asp:RequiredFieldValidator>
                <br>
                 
                <asp:TextBox runat="server" id="pscode" placeholder="Postal-Code"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Postal-Code" ControlToValidate="pscode" id="validatorpscode"></asp:RequiredFieldValidator>
                <br>
                
                <asp:label id ="countryname" runat="server" text="Country Name:"></asp:label>
                
                <asp:DropDownList runat="server" ID="country">
                <asp:ListItem Value="In" Text="India"></asp:ListItem>
                <asp:ListItem Value="Ca" Text="Canada"></asp:ListItem>
                <asp:ListItem Value="Us" Text="USA"></asp:ListItem>
                <asp:ListItem Value="Br" Text="Brazil"></asp:ListItem>
                <asp:ListItem Value="Uk" Text="UK"></asp:ListItem>
                <asp:ListItem Value="Aus" Text="Australia"></asp:ListItem>
                <asp:ListItem Value="Nz" Text="New Zeland"></asp:ListItem>
                </asp:DropDownList>
                <br>
                <br>
                
                <asp:label id ="email" runat="server" text="Email-ID:"></asp:label>
                <asp:TextBox runat="server" ID="txtemail" placeholder="Email"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="txtemail" ID="RequiredFieldValidatortxtemail"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server"  ID="RegularFieldValidatortxtemail"ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
                <br>
                <br>
                
                <asp:label id ="phone" runat="server" text="Phone Number:"></asp:label>   
                <asp:TextBox runat="server" id="txtphone" placeholder="Phone"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="txtphone" ID="validatortxtphone"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" runat="server" ErrorMessage="Enter valid Phone number" ControlToValidate="txtphone" ValidationExpression= "^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>
                <br>
                
                <asp:label id="person" runat="server" text="No. of Person:" ></asp:label>
                <asp:TextBox runat="server" id="txtperson" placeholder=""></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter No. of Person" ControlToValidate="txtperson" ID="validatortxtperson"></asp:RequiredFieldValidator>
                <asp:RangeValidator Enable="false" id="RangeValidatortxtchktime" runat="server" ControlToValidate="txtperson" MinimumValue="1" Type="Integer" MaximumValue="4" ErrorMessage="No. of Person must be between 1 to 4"></asp:RangeValidator>
                <br>
                <br>
                <br>
         
                <asp:label id="paymenttype" runat="server" text="Payment Type:" ></asp:label>
                <asp:RadioButton runat="server" Text="Check" GroupName="Payment"></asp:RadioButton>
                <asp:RadioButton runat="server" Text="Paypal" GroupName="Payment"></asp:RadioButton>
                <br>
                <br>
                
                <asp:label id="roomtype" runat="server" text="Room Type:" ></asp:label>
                <asp:RadioButton runat="server" Text="SuperDelux" GroupName="Room"></asp:RadioButton>
                <asp:RadioButton runat="server" Text="Delux" GroupName="Room"></asp:RadioButton>
                <asp:RadioButton runat="server" Text="Family" GroupName="Room"></asp:RadioButton>
                <br>
                <br>
                
                <asp:label id="propertytype" runat="server" text="Property Type:" ></asp:label>
                <asp:CheckBox runat="server" id="Apt" Text="Apartments"></asp:CheckBox>
                <asp:CheckBox runat="server" id="Gh" Text="GuestHouses"></asp:CheckBox>
                <asp:CheckBox runat="server" id="Hs" Text="HomeStays"></asp:CheckBox>
                <asp:CheckBox runat="server" id="Vh" Text="Vacation Homes"></asp:CheckBox>
                <br>
                <br>
                
                
                <asp:Label id="facility" runat="server" text="Facility:"></asp:Label>
                <asp:CheckBox runat="server" id="Fc" Text="Fitness Center"></asp:CheckBox>
                <asp:CheckBox runat="server" id="NS" Text="Non-Smoking Rooms"></asp:CheckBox>
                <asp:CheckBox runat="server" id="WiFi" Text="Free WiFi"></asp:CheckBox>
                <asp:CheckBox runat="server" id="Pa" Text="Parking"></asp:CheckBox>
                <asp:CheckBox runat="server" id="Re" Text="Restaurant"></asp:CheckBox>
                <asp:CheckBox runat="server" id="As" Text="Airport Shuttle"></asp:CheckBox>
                <asp:CheckBox runat="server" id="Dg" Text="Facilities for Disabled Guests"></asp:CheckBox>
                <br>
                <br>
                
                <asp:Label id="star" runat="server" Text="Star Rating:"></asp:Label>
                <asp:RadioButton runat="server" id="S1" Text="1 Star" GroupName="Star"></asp:RadioButton>
                <asp:RadioButton runat="server" id="S2" Text="2 Star" GroupName="Star"></asp:RadioButton>
                <asp:RadioButton runat="server" id="S3" Text="3 Star" GroupName="Star"></asp:RadioButton>
                <asp:RadioButton runat="server" id="S4" Text="4 Star" GroupName="Star"></asp:RadioButton>
                <asp:RadioButton runat="server" id="S5" Text="5 Star" GroupName="Star"></asp:RadioButton>
                <br>
                
                <div>
                 <asp:ValidationSummary id="validationsummary1" runat="server" HeaderText="Please fill up following Details :"
                  ShowMessageBox="false" DisplayMode="BulletList" ShowSummary="true" BackColor="SkyBlue" Width="400" ForeColor="Black"
                   Font-Size="small" Font-Bold="true" />
                    
                </div>
                
                
                
                <asp:Button id="btnsubmit" runat="server" Text="Submit" ></asp:Button>
       </div>   
            
            
            
	</form>
</body>
</html>
